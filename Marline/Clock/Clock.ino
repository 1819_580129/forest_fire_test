/* 
 * Arduino 101: timer and interrupts
 * 2: Timer1 overflow interrupt example 
 * more infos: http://www.letmakerobots.com/node/28278
 * created by RobotFreak 
 */

#define ledPin 3

char cnt1min = 120;         // 120 x 0.5 sec
char cntSampleTime;         // time counter sample time
char cnt1hour = 60;         // 120 x 1 min
char cnt4hour = 4;          // 4 x 1 hour

void setup()
{
  pinMode(ledPin, OUTPUT);
  init_timer();  
}

void loop()
{
  // your program here...
}

void init_timer(){
  // initialize timer1 
  noInterrupts();           // disable all interrupts
  TCCR1A = 0;
  TCCR1B = 0;

  TCNT1 = 34286;            // preload timer 65536-16MHz/256/2Hz
  TCCR1B |= (1 << CS12);    // 256 prescaler 
  TIMSK1 |= (1 << TOIE1);   // enable timer overflow interrupt
  interrupts();             // enable all interrupts
}


ISR(TIMER1_OVF_vect)        // 0.5 sec = 2Hz
{
  TCNT1 = 34286;            // preload timer
  digitalWrite(ledPin, digitalRead(ledPin) ^ 1);
  
  if(cnt1min > 0){cnt1min--;}

  if(cnt1min == 0){
    cnt1min = 120;

    if(cnt1hour > 0){cnt1hour--;}   
  }

  if(cnt1hour == 0){
    cnt1hour = 60;

    if(cnt4hour > 0){cnt4hour--;}   
  }

  if(cnt4hour == 0){
    cnt4hour = 4;

    //send alive message   
  }
}
