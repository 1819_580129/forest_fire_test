#include <TheThingsNetwork.h>
#include <CayenneLPP.h>

// Set your AppEUI and AppKey console fire detection network
const char *appEui = "70B3D57ED0019737";
const char *appKey = "1A2D3379A350D6D8C735643E9498662B";

#define loraSerial Serial1
#define debugSerial Serial
#define freqPlan TTN_FP_EU868

TheThingsNetwork ttn(loraSerial, debugSerial, freqPlan);

CayenneLPP lpp(51);

int pot1_hum = A0;
int pot2_temp = A1;

 struct Data_sample_time
 {
  signed int actual_co2 = 1500;
  signed int actual_h2o = 50;
  signed int actual_temp = 28;
  signed int min_h2o = 10;
  signed int max_h2o = 60;
  signed int min_temp = 0;
  signed int max_temp = 40;
  signed int min_sample_time = 5; 
  signed int max_sample_time = 60;
  signed int fire = 0;//0 = no fire, 1 = fire
 };

 typedef struct Data_sample_time data;
 struct Data_sample_time *Pdata;
 data Data;
 
 
void setup() {
  loraSerial.begin(57600);
  debugSerial.begin(9600);
  Pdata = &Data;

  debugSerial.println("-- STATUS");
  ttn.showStatus();

  debugSerial.println("-- JOIN");
  ttn.join(appEui, appKey);
}

void loop() {
  Data.actual_h2o = (analogRead(pot1_hum)/10.23);
  Data.actual_temp = ((analogRead(pot2_temp)/10.23) - 50);

  debugSerial.print(Pdata->actual_h2o); 
  debugSerial.print(" RH"); 
  debugSerial.print("\n");
  
  debugSerial.print(Pdata->actual_temp); 
  debugSerial.print(" °C"); 
  debugSerial.print("\n");

  debugSerial.print(calculate_sample_time(Pdata));
  debugSerial.print(" min"); 
  debugSerial.print("\n\n"); 
  //send_data(Pdata);
  delay(2000);
}


/*
 * function that calculates the sensor sample time
 * input: 
 * h2o => int, example: 80.3% = 80
 * temp => int, example: 37.4°C = 37
 * sample time => int
 * output: sample time => int
 */
int calculate_sample_time(struct Data_sample_time *p)
{ 
    float ratio_h2o, ratio_temp, ratio;
    signed int sample_time;//, actual_h2o, actual_temp, min_h2o, max_h2o, min_temp, max_temp, min_sample_time, max_sample_time;
 
    if(p->actual_h2o < p->min_h2o){p->actual_h2o = p->min_h2o;} //overflow/underflow protection
    if(p->actual_h2o > p->max_h2o){p->actual_h2o = p->max_h2o;} //overflow/underflow protection
    if(p->actual_temp < p->min_temp){p->actual_temp = p->min_temp;} //overflow/underflow protection
    if(p->actual_temp > p->max_temp){p->actual_temp = p->max_temp;} //overflow/underflow protection
    
    ratio_h2o = ((float)p->actual_h2o / (float)p->max_h2o);
    ratio_temp = (1 - ((float)p->actual_temp / (float)p->max_temp));
  
    ratio = (ratio_h2o + ratio_temp) / 2;
    
    sample_time = (int)((p->max_sample_time - p->min_sample_time) * ratio);
    sample_time = (int)(sample_time + p->min_sample_time);
    
    return sample_time;
}

void send_data(struct Data_sample_time *p)
{
  lpp.reset();
  lpp.addLuminosity(1, p->actual_co2);
  lpp.addLuminosity(2, p->actual_h2o);
  lpp.addLuminosity(3, p->actual_temp);
  lpp.addLuminosity(4, p->fire);

  ttn.sendBytes(lpp.getBuffer(), lpp.getSize());
}
