/*!
  !Natural trigger used for simulating a sensor with the use of a potmeter
  -------------------------------------------------------------------------*/
/* values need to be added. This will only work for one sensor at a time
 * 
 */


//Analog Read with Serial Monitor
int PIN_LED_1 = 3;
int PIN_POT_1 = A1;

void setup() {
  //the setup routine runs once when you press reset

  Serial.begin(9600); //initialize serial communication at 9600 bits per second

  pinMode(PIN_LED_1, OUTPUT); //initialize digital pin LED_BUILTIN as an output
  pinMode(PIN_POT_1, INPUT);

  digitalWrite(PIN_LED_1, HIGH);  //signle blink test to see if the LED works
  delay(1000);
  digitalWrite(PIN_LED_1, LOW);
}
void loop(){
//bool functionTrigger() {                             //the loop routine runs over and over again forever
  bool alarmTrigger = false;
  
  int potValue = analogRead(PIN_POT_1)/4; //read the input on analog pin 0 0-1023 is mapped to 0 - 255

  Serial.println(potValue);               //print out the value you read
  
  if (potValue > 200){                    //if trigger is reached, start the sequence--->this needs to be changed

  digitalWrite(PIN_LED_1, HIGH);
  alarmTrigger = true;
 

  }
  
return alarmTrigger;
  
//}
}
