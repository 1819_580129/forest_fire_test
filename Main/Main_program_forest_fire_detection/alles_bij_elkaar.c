#include <TheThingsNetwork.h>
#include <CayenneLPP.h>

// Set your AppEUI and AppKey console fire detection network
const char *appEui = "70B3D57ED0019737";
const char *appKey = "1A2D3379A350D6D8C735643E9498662B";

#define loraSerial Serial1
#define debugSerial Serial
#define freqPlan TTN_FP_EU868

TheThingsNetwork ttn(loraSerial, debugSerial, freqPlan);

CayenneLPP lpp(51);

int pot1_hum = A0;
int pot2_temp = A1;

bool flag_data, flag_warning, flag_alarm;

struct Data_sample_time
{
  signed int actual_co2 = 1500;
  signed int actual_h2o = 50;
  signed int actual_temp = 28;
  signed int min_h2o = 10;
  signed int max_h2o = 60;
  signed int min_temp = 0;
  signed int max_temp = 40;
  signed int min_sample_time = 5;
  signed int max_sample_time = 60;
  int de_fik_drin = 0; //0 = goed, 1 = warning, 2 = fikkie
} ;

typedef struct Data_sample_time data;
struct Data_sample_time *Pdata;

data Data;

#define ledPin 3

struct Timer_vars
{
  //simulation mode
  char cnt1sec_sim = 2;           // 2 x 0.5 sec
  char cntSampleTime_sim;         // time counter sample time in seconds simulation
  char cnt1min_sim = 60;          // 60 x 1 sec
  char cnt4min_sim = 4;           // 4 x 1 min
  //normal mode
  char cnt1min = 120;         // 120 x 0.5 sec
  char cntSampleTime;         // time counter sample time in minutes
  char cnt1hour = 60;         // 120 x 1 min
  char cnt4hour = 4;          // 4 x 1 hour
};

typedef struct Timer_vars Timer_Vars;
Timer_Vars TmrVars;

//char cnt1min = 120;         // 120 x 0.5 sec
//char cntSampleTime;         // time counter sample time
//char cnt1hour = 60;         // 120 x 1 min
//char cnt4hour = 4;          // 4 x 1 hour

bool SimulationState = false; //gekopieerd van Jaap

void setup() {
  pinMode(ledPin, OUTPUT);

  /////copy////////
  init_timer(); //init timer 1 (hardware timer)
  loraSerial.begin(57600);
  debugSerial.begin(9600);
  Pdata = &Data;

  debugSerial.println("-- STATUS");
  ttn.showStatus();

  debugSerial.println("-- JOIN");
  ttn.join(appEui, appKey);

  TmrVars.cntSampleTime = calculate_sample_time(Pdata); //calculate sample time first time to fill timer var
  /////end copy////////
}

void loop() {
  //test//
  Data.actual_h2o = (analogRead(pot1_hum)/10.23);             //
  Data.actual_temp = ((analogRead(pot2_temp)/10.23) - 50);

  debugSerial.print(Pdata->actual_h2o);
  debugSerial.print(" RH");
  debugSerial.print("\n");

  debugSerial.print(Pdata->actual_temp);
  debugSerial.print(" °C");
  debugSerial.print("\n");

  debugSerial.print(calculate_sample_time(Pdata));
  debugSerial.print(" min");
  debugSerial.print("\n\n");
  //test//

  /////copy////////
  if(flag_data || flag_warning || flag_alarm){          //if one of the flags is 1 then send data(flag are to prevent two data strings at the same time
    flag_data = 0; flag_warning = 0; flag_alarm = 0;    //Reset all flags
    send_data(Pdata);                                   //Send actual data to lorawan console
  }
  /////end copy////////

  delay(2000);
}


/*
 * function that calculates the sensor sample time
 * input:
 * h2o => int, example: 80.3% = 80
 * temp => int, example: 37.4°C = 37
 * sample time => int
 * output: sample time => int
 */
int calculate_sample_time(struct Data_sample_time *p)
{
  float ratio_h2o, ratio_temp, ratio;
  signed int sample_time;

  if(p->actual_h2o < p->min_h2o){p->actual_h2o = p->min_h2o;}             //overflow/underflow protection
  if(p->actual_h2o > p->max_h2o){p->actual_h2o = p->max_h2o;}             //overflow/underflow protection
  if(p->actual_temp < p->min_temp){p->actual_temp = p->min_temp;}         //overflow/underflow protection
  if(p->actual_temp > p->max_temp){p->actual_temp = p->max_temp;}         //overflow/underflow protection

  //low humidity = high risk = low ratio, low temp = low risk = high ratio
  ratio_h2o = ((float)p->actual_h2o / (float)p->max_h2o);                 //calculate ratio relative humidity
  ratio_temp = (1 - ((float)p->actual_temp / (float)p->max_temp));        //calculate inverse ratio temperature

  ratio = (ratio_h2o + ratio_temp) / 2;                                   //average ratio

  sample_time = (int)((p->max_sample_time - p->min_sample_time) * ratio); //calculate ratio between min and max sample time
  sample_time = (int)(sample_time + p->min_sample_time);                  //add minimum sample time

  return sample_time;
}

void send_data(struct Data_sample_time *p)
{
  lpp.reset();                            //reset buffer
  lpp.addLuminosity(1, p->actual_co2);    //fill buffer co2 in luminosity(integer)
  lpp.addLuminosity(2, p->actual_h2o);    //fill buffer h2o in luminosity(integer)
  lpp.addLuminosity(3, p->actual_temp);   //fill buffer temp in luminosity(integer)
  lpp.addLuminosity(4, p->de_fik_drin);   //fill buffer de_fik in luminosity(integer)

  ttn.sendBytes(lpp.getBuffer(), lpp.getSize()); //send data
}

void init_timer(){
  // initialize timer1
  noInterrupts();           // disable all interrupts
  TCCR1A = 0;
  TCCR1B = 0;

  TCNT1 = 34286;            // preload timer 65536-16MHz/256/2Hz
  TCCR1B |= (1 << CS12);    // 256 prescaler
  TIMSK1 |= (1 << TOIE1);   // enable timer overflow interrupt
  interrupts();             // enable all interrupts
}


ISR(TIMER1_OVF_vect)        // 0.5 sec = 2Hz
{
  TCNT1 = 34286;            // preload timer
  digitalWrite(ledPin, digitalRead(ledPin) ^ 1);

char cnt1sec_sim = 2;           // 2 x 0.5 sec
  char cntSampleTime_sim;         // time counter sample time
  char cnt1min_sim = 60;          // 60 x 1 sec
  char cnt4min_sim = 4;           // 4 x 1 min

  if(SimulationState){ //simulation mode
    if(TmrVars.cnt1sec_sim > 0){TmrVars.cnt1sec_sim--;}

    if(TmrVars.cnt1sec_sim == 0){
      TmrVars.cnt1sec_sim = 2;

      if(TmrVars.cnt1min_sim > 0){TmrVars.cnt1min_sim--;}
      if(TmrVars.cntSampleTime_sim > 0){TmrVars.cntSampleTime_sim--;}
    }

    if(TmrVars.cntSampleTime_sim == 0){
      TmrVars.cntSampleTime_sim = calculate_sample_time(Pdata);
      //naar de volgende state verwijzen (functie is gewoon voorbeeld)
    }

    if(TmrVars.cnt1min_sim == 0){
      TmrVars.cnt1min_sim = 60;

      if(TmrVars.cnt4min_sim > 0){TmrVars.cnt4min_sim--;}
    }

    if(TmrVars.cnt4min_sim == 0){
      TmrVars.cnt4min_sim = 4;
      //set flag to send alive message
      flag_data = true;
    }
  }
  else{ //normal mode
    if(TmrVars.cnt1min > 0){TmrVars.cnt1min--;}

    if(TmrVars.cnt1min == 0){
      TmrVars.cnt1min = 120;

      if(TmrVars.cnt1hour > 0){TmrVars.cnt1hour--;}
      if(TmrVars.cntSampleTime > 0){TmrVars.cntSampleTime--;}
    }

    if(TmrVars.cntSampleTime == 0){
      TmrVars.cntSampleTime = calculate_sample_time(Pdata);
    }

    if(TmrVars.cnt1hour == 0){
      TmrVars.cnt1hour = 60;

      if(TmrVars.cnt4hour > 0){TmrVars.cnt4hour--;}
    }

    if(TmrVars.cnt4hour == 0){
      TmrVars.cnt4hour = 4;
      //set flag to send alive message
      flag_data = true;
    }
  }
}
