/*--------------------------------------------------------------------
  This file is part of the HAN IoT shield library.

  This code is free software:
  you can redistribute it and/or modify it under the terms of a Creative
  Commons Attribution-NonCommercial 4.0 International License
  (http://creativecommons.org/licenses/by-nc/4.0/) by
  Remko Welling (https://ese.han.nl/~rwelling/) E-mail: remko.welling@han.nl

  The program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  --------------------------------------------------------------------*/

/*!
   \file SFFA_TestSignalGenerator_003.ino
   \brief Sends packets on TTN using the HAN IoT Shield.
   this sketch is made to demonstrate the functionalities of the IoT Shield
   that is used in the IoT class of Embedded Systems. It delivers a library
   to allow studens to easily use the IoT shield and explore the interfaces
   that are used to work with the IoT shield.
   \author Remko Welling (remko.welling@han.nl)
   \date 14-1-2019
   \version 7

   Version|Date        |Note
   -------|------------|----
   8      |  4- 2-2019 | .........................................................................
   7      | 14- 1-2019 | .........................................................................
          |            | .........................................................................
   6      | 10- 1-2019 | .........................................................................
   5      | sdsdsdsdsd | First released version

*/

#include <TheThingsNetwork.h>
#include <CayenneLPP.h>

// include Headers
#include "defines.h"
#include "bools.h"
#include "simulation.c"
#include "Global_Data.h"

const char *appEui = "70B3D57ED0019737";
const char *appKey = "0317A9B0B8D471CC721077C34D19C7C1";

TheThingsNetwork ttn(loraSerial, debugSerial, freqPlan);

int pot1_hum = A0;
int pot2_temp = A1;

typedef enum {MEASURE, ALARM_CHECK,
              CALCULATE_WAITING_TIME,
              WAITING,
              SEND_ALARM,
              SEND_IM_ALIVE
             } state_t;                              // state definities

int state = MEASURE;

typedef struct Data data;
struct Data *Pdata;

data Data;

void setup() {
  init_timer();                                         //init timer 1 (hardware timer)
  loraSerial.begin(57600);
  debugSerial.begin(9600);
Serial.begin(9600);

  Pdata = &Data;

  pinMode(pot1_hum, INPUT);
  pinMode(pot2_temp, INPUT);
  
  // Initialize digital pin LED_BUILTIN as an output.
  pinMode(PIN_LED_1, OUTPUT);
  pinMode(PIN_LED_2, OUTPUT);
  pinMode(PIN_LED_3, OUTPUT);
  pinMode(PIN_LED_4, OUTPUT);

  // Initialize the pushbutton pin as an input:
  pinMode(PIN_SWITCH_BLACK, INPUT);
  pinMode(PIN_SWITCH_RED, INPUT);

  debugSerial.println("-- STATUS");
  ttn.showStatus();

  debugSerial.println("-- JOIN");
  ttn.join(appEui, appKey);
}

void loop() {

  while (1) {
    switch (state) {
      case MEASURE:                                                                                    // zo goed as klaar
        debugSerial.println(F("measure"));                 // wordt later een functie
        measure();

        debugSerial.print(" RH: ");
        debugSerial.print(Pdata.actual_h2o);
        debugSerial.print(" ----- RH input: ");
        debugSerial.print(analogRead(pot1_hum));
        debugSerial.print("\n");

        debugSerial.print(" °C :");
        debugSerial.print(Pdata->actual_temp);
        debugSerial.print(" ----- °C input: ");
        debugSerial.print(analogRead(pot2_temp));
        debugSerial.print("\n");

        state = ALARM_CHECK;
        break;

      case ALARM_CHECK:
        debugSerial.println(F("Alarm_Check"));                 // wordt later een functie

        // functie alarm check

        if (flag_Alarm_Warning) {
          flag_Alarm_Warning = false;
          state = SEND_ALARM;
        }
        else {
          state = CALCULATE_WAITING_TIME;
        }
        break;

      case CALCULATE_WAITING_TIME:
        debugSerial.println(F("Calculate wating time:"));

        calculate_sample_time(Pdata);

        debugSerial.print(" After executing calculate_sample_time:\n");
        debugSerial.print(" RH: ");
        debugSerial.print(Pdata->actual_h2o);
        debugSerial.print("\n");

        debugSerial.print(" °C :");
        debugSerial.print(Pdata->actual_temp);
        debugSerial.print("\n");

        debugSerial.print(" Waiting time: ");
        debugSerial.print(Pdata->waiting_time);
        debugSerial.print("\n");

        TmrVars.cntSampleTime_norm = waiting_time;
        TmrVars.cntSampleTime_sim = waiting_time;

      state = WAITING;
      break;

    case WAITING:
      debugSerial.println(F("Waiting"));

      if (flag_cntSampleTime) {
        state = MEASURE;
        flag_cntSampleTime = false;
        break;

      }
      else if (flag_cntImAliveTime) {
        flag_cntImAliveTime = false;
        state = SEND_IM_ALIVE;
        break;

      }

      break;

    case SEND_ALARM:
      debugSerial.println(F("Send_alarm"));                 // wordt later een functie
      //             send_Alarm()
      //           Reset Im Alive Counter
      state = WAITING;
      break;

    case SEND_IM_ALIVE:
      debugSerial.println(F("Send im alive"));                 // wordt later een functie
      //             send_ImAlive();

      state = WAITING;
      break;

    }
  }
}

void measure(void) {

  Data.actual_h2o = (hum_sensor_max - (analogRead(pot1_hum) / ((double)1023 / (hum_sensor_max - hum_sensor_min))));
  Data.actual_temp = (temp_sensor_max - (analogRead(pot2_temp) / ((double)1023 / (temp_sensor_max - temp_sensor_min))));

}

/*
   function that calculates the sensor sample time
   input:
   h2o => int, example: 80.3% = 80
   temp => int, example: 37.4°C = 37
   sample time => int
   output: sample time => int
*/

void calculate_sample_time(struct Data * p)
{
  float ratio_h2o, ratio_temp, ratio;
  signed int sample_time;

  if (p->actual_h2o < p->min_h2o) {
    p->actual_h2o = p->min_h2o; //overflow/underflow protection
  }
  if (p->actual_h2o > p->max_h2o) {
    p->actual_h2o = p->max_h2o; //overflow/underflow protection
  }
  if (p->actual_temp < p->min_temp) {
    p->actual_temp = p->min_temp; //overflow/underflow protection
  }
  if (p->actual_temp > p->max_temp) {
    p->actual_temp = p->max_temp; //overflow/underflow protection
  }

  //low humidity = high risk = low ratio, low temp = low risk = high ratio
  ratio_h2o = ((float)p->actual_h2o / (float)p->max_h2o);                 //calculate ratio relative humidity
  ratio_temp = (1 - ((float)p->actual_temp / (float)p->max_temp));        //calculate inverse ratio temperature

  ratio = (ratio_h2o + ratio_temp) / 2;                                   //average ratio

  sample_time = (int)((p->max_sample_time - p->min_sample_time) * ratio); //calculate ratio between min and max sample time
  sample_time = (int)(sample_time + p->min_sample_time);                  //add minimum sample time

  p->waiting_time = sample_time;

}

struct Timer_vars
{
  //simulation mode
  char cnt1sec_sim = 2;           // 2 x 0.5 sec
  char cntSampleTime_sim;         // time counter sample time in seconds simulation
  char cnt1min_sim = 60;          // 60 x 1 sec
  char cnt4min_sim = 4;           // 4 x 1 min
  //normal mode
  char cnt1min = 120;         // 120 x 0.5 sec
  char cntSampleTime_norm;         // time counter sample time in minutes
  char cnt1hour = 60;         // 120 x 1 min
  char cnt4hour = 4;          // 4 x 1 hour
};

typedef struct Timer_vars Timer_Vars;
Timer_Vars TmrVars;

void init_timer() {
  // initialize timer1
  noInterrupts();           // disable all interrupts
  TCCR1A = 0;
  TCCR1B = 0;

  TCNT1 = 34286;            // preload timer 65536-16MHz/256/2Hz
  TCCR1B |= (1 << CS12);    // 256 prescaler
  TIMSK1 |= (1 << TOIE1);   // enable timer overflow interrupt
  interrupts();             // enable all interrupts
}

ISR(TIMER1_OVF_vect)        // 0.5 sec = 2Hz
{
  TCNT1 = 34286;            // preload timer          34286
  digitalWrite(PIN_LED_1, digitalRead(PIN_LED_1) ^ 1);

// char cnt1sec_sim = 2;           // 2 x 0.5 sec
//  char cntSampleTime_sim;         // time counter sample time
//  char cnt1min_sim = 60;          // 60 x 1 sec
//  char cnt4min_sim = 4;           // 4 x 1 min

  if (SimulationState) {          //simulation mode
    if (TmrVars.cnt1sec_sim > 0) {
      TmrVars.cnt1sec_sim--;
    }

    if (TmrVars.cnt1sec_sim == 0) {
      TmrVars.cnt1sec_sim = 2;

      if (TmrVars.cnt1min_sim > 0) {
        TmrVars.cnt1min_sim--;
      }
      if (TmrVars.cntSampleTime_sim > 0) {
        TmrVars.cntSampleTime_sim--;
      }
    }

    if (TmrVars.cntSampleTime_sim == 0) {
      flag_cntSampleTime = true;                         //set flag to go to the next state
    }

    if (TmrVars.cnt1min_sim == 0) {
      TmrVars.cnt1min_sim = 60;

      if (TmrVars.cnt4min_sim > 0) {
        TmrVars.cnt4min_sim--;
      }
    }

    if (TmrVars.cnt4min_sim == 0) {
      TmrVars.cnt4min_sim = 4;

      flag_cntImAliveTime = true;                         //set flag to go to the next state
    }
  }
  else { //normal mode
    if (TmrVars.cnt1min > 0) {
      TmrVars.cnt1min--;
    }

    if (TmrVars.cnt1min == 0) {
      TmrVars.cnt1min = 120;

      if (TmrVars.cnt1hour > 0) {
        TmrVars.cnt1hour--;
      }
      if (TmrVars.cntSampleTime_norm > 0) {
        TmrVars.cntSampleTime_norm--;
      }
    }

    if (TmrVars.cntSampleTime_norm == 0) {
      flag_cntSampleTime = true;                         //set flag to go to the next state
    }

    if (TmrVars.cnt1hour == 0) {
      TmrVars.cnt1hour = 60;

      if (TmrVars.cnt4hour > 0) {
        TmrVars.cnt4hour--;
      }
    }

    if (TmrVars.cnt4hour == 0) {
      TmrVars.cnt4hour = 4;

      flag_cntImAliveTime = true;                         //set flag to go to the next state
    }
  }
}
