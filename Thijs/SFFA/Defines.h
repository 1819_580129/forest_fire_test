#ifndef DEFINES
#define DEFINES

// All defines
#define loraSerial Serial1
#define debugSerial Serial
#define freqPlan TTN_FP_EU868

///define for LEDs
#define PIN_LED_1    3     ///< Led 1 is red and connected to arduino pin 3
#define PIN_LED_2    4     ///< Led 2 is red and connected to arduino pin 4
#define PIN_LED_3    5     ///< Led 3 is green and connected to arduino pin 5
#define PIN_LED_4    6     ///< Led 4 is green and connected to arduino pin 6


///define of the potmeters red and white
#define PIN_POT_RED      A0         ///< Potmeter 1 with red knob is connected to Arduino pin A0
#define PIN_POT_WHITE    A1         ///< Potmeter 2 with white knob is connected to Arduino pin A1

///define for pushbutton
#define PIN_SWITCH_BLACK 8     ///< Black switch is connected to pin Arduino pin 8
#define PIN_SWITCH_RED   9     ///< Red switch is connected to pin Arduino pin 9
#define RELEASED HIGH          ///< represent activity of pushbutton, Released is true
#define PRESSED  LOW           ///< represent activity of pushbutton, Pressed is false

#define PIN_LED_1 3

#endif
