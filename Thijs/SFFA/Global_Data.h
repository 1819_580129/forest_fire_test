#ifndef GLOBAL_DATA
#define GLOBAL_DATA

#define hum_sensor_min 0
#define hum_sensor_max 100
#define temp_sensor_min -50
#define temp_sensor_max 50
#define co2_sensor_min ??
#define co2_sensor_max ??

struct Data
{
  signed int actual_co2 = 1500;
  signed int actual_h2o = 50;
  signed int actual_temp = 28;
  signed int min_h2o = 10;
  signed int max_h2o = 60;
  signed int min_temp = 0;
  signed int max_temp = 40;
  signed int min_sample_time = 5;
  signed int max_sample_time = 60;
  signed int waiting_time = min_sample_time;
  int de_fik_drin = 0; //0 = goed, 1 = warning, 2 = fikkie
} ;

#endif
