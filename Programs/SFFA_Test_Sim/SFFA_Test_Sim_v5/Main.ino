#include <stdio.h>
#include <time.h>


void delay(int milliseconds);                      // vervalt, wordt vervangen door
                                                   // een andere functie

void setup() {
  Serial.begin(9600);
  
// Initialize digital pin LED_BUILTIN as an output.
  pinMode(PIN_LED_1, OUTPUT);
  pinMode(PIN_LED_2, OUTPUT);
  pinMode(PIN_LED_3, OUTPUT);
  pinMode(PIN_LED_4, OUTPUT);
  
// Initialize the pushbutton pin as an input:
  pinMode(PIN_SWITCH_BLACK, INPUT);
  pinMode(PIN_SWITCH_RED, INPUT);

}
                                                   
int main(void)
{

   while (1){
      switch(state){
         case MEASURE:
            printf("measure\n\n");                 // wordt later een functie
            state = ALARM_CHECK;
            break;

         case ALARM_CHECK:
            printf("alarm check\n\n");             // wordt later een functie
            if (high_temp){state = SEND_ALARM;}
            else {state = CALCULATE_WAITING_TIME;}
            break;

         case CALCULATE_WAITING_TIME:
             printf("calculate waiting time\n\n"); // wordt later een functie
             state = WAITING;

            break;
         case WAITING:
             printf("waiting\n\n");          // wordt later een functie
             if(timer_ends){state = SEND_IM_ALIVE; break;}
            delay(6000);
            state = MEASURE;
            break;

         case SEND_ALARM:
             printf("send alarm\n\n");       // wordt later een functie
            state = WAITING;
            break;

         case SEND_IM_ALIVE:
            printf("send i'm alive\n\n");        // wordt later een functie
            timer_ends = 0;
            state = WAITING;
            break;
}
}
}

void delay(int milliseconds)
{
    long pause;
    clock_t now,then;

    pause = milliseconds*(CLOCKS_PER_SEC/1000);
    now = then = clock();
    while( (now-then) < pause )
        now = clock();
}
