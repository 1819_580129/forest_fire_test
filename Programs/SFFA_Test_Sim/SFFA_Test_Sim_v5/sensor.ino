//Analog Read with Serial Monitor
int PIN_LED_1 = A0;
int PIN_POT_1 = A1;

void setup() {
  //the setup routine runs once when you press reset

  Serial.begin(9600); //initialize serial communication at 9600 bits per second
  
    pinMode(PIN_LED_1, OUTPUT); //initialize digital pin LED_BUILTIN as an output

      pinMode(/*need a trigger point readout here*/), INPUT;
}


void loop() {
  //the loop routine runs over and over again forever

  int potValue = analogRead(A0); //read the input on analog pin 0
  analogWrite(PIN_LED_1, potValue//??); //<-- potmeter value (0-1023 and led value??)
  Serial.println(potValue); //print out the value you read

  delay(1); //delay in between reads for stability
}
