var indexSectionsWithContent =
{
  0: "acdlmprsw",
  1: "d",
  2: "lms",
  3: "lrw",
  4: "admps",
  5: "d",
  6: "c",
  7: "cdsw",
  8: "pr",
  9: "s"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros",
  9: "Pages"
};

