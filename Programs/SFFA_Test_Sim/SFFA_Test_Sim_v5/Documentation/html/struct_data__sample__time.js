var struct_data__sample__time =
[
    [ "actual_co2", "struct_data__sample__time.html#a78181520936fd105de0f4832586df1dc", null ],
    [ "actual_h2o", "struct_data__sample__time.html#acd005461388adc525573a085083ece9f", null ],
    [ "actual_temp", "struct_data__sample__time.html#a22c541fe72d6dcec91f9728790b315af", null ],
    [ "de_fik_drin", "struct_data__sample__time.html#a114e8ac9d324f5a1597b12f68e5bdc89", null ],
    [ "max_h2o", "struct_data__sample__time.html#a30479fa59ca184d729bdab1540206981", null ],
    [ "max_sample_time", "struct_data__sample__time.html#a01a545859d126dde09e8fd6f2866ebb1", null ],
    [ "max_temp", "struct_data__sample__time.html#ad6fd72bd05356489268df485464dc528", null ],
    [ "min_h2o", "struct_data__sample__time.html#a70e877016dba43e069a0666b671e2579", null ],
    [ "min_sample_time", "struct_data__sample__time.html#a080700b7e0726da733a6f302d2171857", null ],
    [ "min_temp", "struct_data__sample__time.html#ad6154e6fd9e71e692818bca147927dd9", null ]
];