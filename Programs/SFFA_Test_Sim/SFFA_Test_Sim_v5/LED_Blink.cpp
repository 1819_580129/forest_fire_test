
/*!
 *
 * file LED_Blink.cpp
 * Functions for simulation the SFFA project
 * date 03-04-2019
 * author Jaap van Rooijen
 * version 2.0
 * 
 * \file LED_Blink.cpp
 * \release nr. 2 for simulation of the SFFA project
 * 
 * Writen by Jaap van Rooijen
 * Student HAN ESE 583405
 * email: JJ.vanRooijen@student.han.nl
 * 
 */


//For debugging of temp/h20/co2
//      Serial.println("H20 = ");
//      Serial.print(Data.actual_h2o);
//      Serial.println("  ");
//      Serial.println("C02 = ");
//      Serial.print(Data.actual_co2);
//      Serial.println("  ");
//      Serial.println("Temp = ");
//      Serial.print(Data.actual_temp);
//      Serial.println("  ");

#include "LED_Blink.h"
#include "Arduino.h"


///main datastruct
struct Data_sample_time
{
  signed int actual_co2 = 400;
  signed int actual_h2o = 50;
  signed int actual_temp = 28;
  signed int min_h2o = 10;
  signed int max_h2o = 60;
  signed int min_temp = 0;
  signed int max_temp = 40;
  signed int min_sample_time = 5; 
  signed int max_sample_time = 60;
  int de_fik_drin = 0; //0 = goed, 1 = warning, 2 = fikkie
};

typedef struct Data_sample_time data;
struct Data_sample_time *Pdata;
data Data;

bool SimulationState = false;

//Functions to call
void RedPotMeterToLED(bool SimulationState, struct Data_sample_time *p){
 static int MappedpotRedValue = 0;         ///\ Mapped value depending SimulationState
        int potRedValueRaw    = 0;         ///\ Variable to store value from the potentiometer
  const int potRedMin         = 0;         ///\ Min range for Red potentiometer SimulationState false
  const int potRedMax         = 100;       ///\ Max range for Red potentiometer SimulationState false
  const int potRedMinSim      = 400;       ///\ Min range for Red potentiometer SimulationState true
  const int potRedMaxSim      = 5000;      ///\ Max range for Red potentiometer SimulationState true
  potRedValueRaw = analogRead(PIN_POT_RED);
  
  if (SimulationState == false){
      MappedpotRedValue = map(1023-potRedValueRaw, 0, 1023, potRedMin, potRedMax);
      analogWrite(PIN_LED_3, 1023-potRedValueRaw/4);
      Data.actual_h2o = MappedpotRedValue;
  }
  else{
    MappedpotRedValue = map(1023-potRedValueRaw, 0, 1023, potRedMinSim, potRedMaxSim);
    analogWrite(PIN_LED_3, 1023-potRedValueRaw/4);
    Data.actual_co2 = MappedpotRedValue;
    }
}

void WhitePotMeterToLED(bool SimulationState, struct Data_sample_time *p){
 signed int MappedpotWhiteValue = 0;      ///\ Mapped value depending SimulationState
        int potWhiteValueRaw    = 0;      ///\ Variable to store value from the potentiometer
  const int potWhiteMin         = -20;    ///\ Min range for White potentiometer SimulationState false
  const int potWhiteMax         = 50;     ///\ Max range for White potentiometer SimulationState false
  const int potWhiteMinSim      = 35;     ///\ Min range for White potentiometer SimulationState true
  const int potWhiteMaxSim      = 500;    ///\ Max range for White potentiometer SimulationState true
  potWhiteValueRaw = analogRead(PIN_POT_WHITE);
  
  if (SimulationState == false){
    MappedpotWhiteValue = map(1023-potWhiteValueRaw, 0, 1023, potWhiteMin, potWhiteMax); 
    analogWrite(PIN_LED_4, 1023-potWhiteValueRaw/4);
  }
  else{
    MappedpotWhiteValue = map(1023-potWhiteValueRaw, 0, 1023, potWhiteMinSim, potWhiteMaxSim);
    analogWrite(PIN_LED_4, 1023-potWhiteValueRaw/4);
    }
  Data.actual_temp = MappedpotWhiteValue;
}
    
// Function: LEDblinkAction
//LED flash without the use of delay
void LEDBlinkAction(int CaseCount){
  unsigned long currentMillis = millis();
  static unsigned long previousMillis = 0;
  const long interval = 1000;

 switch(CaseCount) {
  
  case Simulation_start:{
      Serial.println("Simulatie gestart");
      digitalWrite(PIN_LED_1, HIGH);
      WhitePotMeterToLED(SimulationState = false,Pdata);
      RedPotMeterToLED(SimulationState = false, Pdata);
    }
  break;
  
  case Warning_fire_risk:{
    Serial.println("!!WAARSCHUWING verhoogde kans op Brand!!");
    WhitePotMeterToLED(SimulationState = false,Pdata);
    RedPotMeterToLED(SimulationState = false, Pdata);

    if((currentMillis - previousMillis) >= (interval/2)){
          previousMillis = currentMillis;
          if(digitalRead(PIN_LED_1) == LOW){
            digitalWrite(PIN_LED_1, HIGH);
          }
          else{
            digitalWrite(PIN_LED_1, LOW);
          }
       }
    }
  break;
  
  case Detection_fire:{
    Serial.println("!!DETECTION!!");
    WhitePotMeterToLED(SimulationState = true,Pdata);
    RedPotMeterToLED(SimulationState = true, Pdata);

    if((currentMillis - previousMillis) >= (interval/4)){
          previousMillis = currentMillis;
          if(digitalRead(PIN_LED_1) == LOW){
            digitalWrite(PIN_LED_1, HIGH);
            digitalWrite(PIN_LED_2, LOW);
          }
          else{
            digitalWrite(PIN_LED_1, LOW);
            digitalWrite(PIN_LED_2, HIGH);
          }
       }
    }
  break;
  
  case Conformation_fire:{
    Serial.println("!!CONFORMATION!!");
    WhitePotMeterToLED(SimulationState = true,Pdata);
    RedPotMeterToLED(SimulationState = true, Pdata);

     if((currentMillis - previousMillis) >= (interval/10  )){
          previousMillis = currentMillis;
          if(digitalRead(PIN_LED_1) == LOW){
            digitalWrite(PIN_LED_2, LOW);
            digitalWrite(PIN_LED_1, HIGH);
          }
          else{
            digitalWrite(PIN_LED_2, HIGH);
            digitalWrite(PIN_LED_1, LOW);
          }
       }
    }     
  break;
 
  default :{
  digitalWrite(PIN_LED_1, LOW);
  digitalWrite(PIN_LED_2, LOW);
  digitalWrite(PIN_LED_3, LOW);
  digitalWrite(PIN_LED_4, LOW);
  SimulationState = false;
  }
 }
}   
