/*--------------------------------------------------------------------
* update discrition;
  --------------------------------------------------------------------*/

/// \file LED_Blink.cpp
/// \author Jaap van Rooijen HAN ESE minor

#include "LED_Blink.h"
#include "Arduino.h"

//Global var for potmeters
      int potRedValueRaw = 0;         // variable to store the value coming from the potentiometer
      int potWhiteValue  = 0;         // variable to store the value coming from the potentiometer
const int potRedMin      = 0;         // Min range for Red potentiometer
const int potRedMax      = 5000;      // Max range for Red potentiometer
const int potWhiteMin    = -40;       // Min range for White potentiometer
const int potWhiteMax    = 500;       // Max range for White potentiometer
const int stndValue      = 1023;      // Standard value for Red and White potentiometer

///Function to read Red potmeter >> print to serial and visual to led nr 3
void RedPotMeterToLED(){
    potRedValueRaw = analogRead(PIN_POT_RED);
    int MappedpotRedValue = map(stndValue-potRedValueRaw, 0, 1023, potRedMin, potRedMax);
    analogWrite(PIN_LED_3, stndValue - potRedValueRaw / 4);
    Serial.println("C02 in ppm:");
    Serial.print(MappedpotRedValue);
    Serial.println("  ");
}

//Function to read White potmeter >> print to serial and visual to led nr 4
void WhitePotMeterToLED(){
    potRedValueRaw = analogRead(PIN_POT_WHITE);
    int MappedpotWhiteValue = map(stndValue-potRedValueRaw, 0, 1023, potWhiteMin, potWhiteMax); 
    analogWrite(PIN_LED_4, stndValue - potRedValueRaw / 4);
    Serial.println("Temperatuur in °C:");
    Serial.print(MappedpotWhiteValue);
    Serial.println("  ");
}
    
// Function: LEDblinkAction
//LED flash without the use of delay

void LEDBlinkAction(int CaseCount){
  unsigned long currentMillis = millis();
  static unsigned long previousMillis = 0;
  const long interval = 1000;

 switch(CaseCount) {
  case 1:{
     Serial.println("Simulatie gestart");
     digitalWrite(PIN_LED_1, HIGH);
      }
  break;
  
  case 2:{
    Serial.println("!!WAARSCHUWING!!");
    Serial.println("Verhoogde kans op brand!");
    if((currentMillis - previousMillis) >= (interval/2)){
          previousMillis = currentMillis;
          if(digitalRead(PIN_LED_1) == LOW){
            digitalWrite(PIN_LED_1, HIGH);
          }
          else{
            digitalWrite(PIN_LED_1, LOW);
          }
        }
      }
  break;
  
  case 3:{
    Serial.println("!!ALARM!!");
    Serial.println("Brand detectie op C02 gehalte");
    RedPotMeterToLED();
    if((currentMillis - previousMillis) >= (interval/4)){
          previousMillis = currentMillis;
          if(digitalRead(PIN_LED_1) == LOW){
            digitalWrite(PIN_LED_1, HIGH);
            digitalWrite(PIN_LED_2, LOW);
          }
          else{
            digitalWrite(PIN_LED_1, LOW);
            digitalWrite(PIN_LED_2, HIGH);
          }
        }
      }
  break;
  
  case 4:{
    Serial.println("!!ALARM!!");
    Serial.println("Brand detectie op C02 en Temperatuur");
    WhitePotMeterToLED();
    RedPotMeterToLED();
    
     if((currentMillis - previousMillis) >= (interval/10  )){
          previousMillis = currentMillis;
          if(digitalRead(PIN_LED_1) == LOW){
            digitalWrite(PIN_LED_2, LOW);
            digitalWrite(PIN_LED_1, HIGH);
          }
          else{
            digitalWrite(PIN_LED_2, HIGH);
            digitalWrite(PIN_LED_1, LOW);
          }
        }
      }     
  break;
 case 5:{
  Serial.println("You will proceed with normal operation");
  digitalWrite(PIN_LED_1, LOW);
  digitalWrite(PIN_LED_2, LOW);
  digitalWrite(PIN_LED_3, LOW);
  digitalWrite(PIN_LED_4, LOW);
 }
 break;  

  
  default :{
  digitalWrite(PIN_LED_1, LOW);
  digitalWrite(PIN_LED_2, LOW);
  digitalWrite(PIN_LED_3, LOW);
  digitalWrite(PIN_LED_4, LOW);
  }
 }
}   
