/*--------------------------------------------------------------------
  This file is part of the SFFA_test_Sim >v3.
  --------------------------------------------------------------------*/

/*!
   
 \file LED_Blink.h
 \brief LED blink
 \date 22-03-2019
 \author Jaap van Rooijen
 \version 1.1
 
 Version | comment
 --------|--------
 1.0     | Initial version
 1.1     | updated for ShieldPotmeter for v4.0 of SFFA_test_Sim
 
 */
#ifndef __LED_BLINK_H_
#define __LED_BLINK_H_

#include <Arduino.h>

// Defines for LEDs
#define PIN_LED_1    3     ///< Led 1 is red and connected to arduino pin 3
#define PIN_LED_2    4     ///< Led 2 is red and connected to arduino pin 4
#define PIN_LED_3    5     ///< Led 3 is green and connected to arduino pin 5
#define PIN_LED_4    6     ///< Led 4 is green and connected to arduino pin 6


//define of the potmeters red and white
#define PIN_POT_RED      A0         ///< Potmeter 1 with red knob is connected to Arduino pin A0
#define PIN_POT_WHITE    A1         ///< Potmeter 2 with white knob is connected to Arduino pin A1

// Defines for pushbutton
#define PIN_SWITCH_BLACK 8     ///< Black switch is connected to pin Arduino pin 8
#define PIN_SWITCH_RED   9     ///< Red switch is connected to pin Arduino pin 9
#define RELEASED HIGH          ///< represent activity of pushbutton, Released or not pressed is HIGH or '1'
#define PRESSED  LOW           ///< represent activity of pushbutton, Pressed is LOW or '0'

//Function LED blink
void LEDBlinkAction(int);
//Function Potmeters to LEDS
void PotMeterToLED();
//Function Stimulation
void Stimulation();


#endif //__LED_BLINK_H_
