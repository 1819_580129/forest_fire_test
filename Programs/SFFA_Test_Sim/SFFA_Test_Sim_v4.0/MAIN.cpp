#include "LED_Blink.h"


bool IN_TEST_OPERATION = false;
int START_STIMULATION;                 // variable for reading the pushbutton status
           //


void setup() {
  Serial.begin(9600);
  
// Initialize digital pin LED_BUILTIN as an output.
  pinMode(PIN_LED_1, OUTPUT);
  pinMode(PIN_LED_2, OUTPUT);
  pinMode(PIN_LED_3, OUTPUT);
  pinMode(PIN_LED_4, OUTPUT);
  
// Initialize the pushbutton pin as an input:
  pinMode(PIN_SWITCH_BLACK, INPUT);
  pinMode(PIN_SWITCH_RED, INPUT);
}

void loop() {
while (IN_TEST_OPERATION)

// Standard delay otherwise cycletime is to short for reading messages
    delay(80); // remove/adjust when implemented in main file
    
// Read the state of the pushbutton value:
  START_STIMULATION = digitalRead(PIN_SWITCH_RED);
    
//Start Function for Stimulation
    if(START_STIMULATION == PRESSED){
    digitalWrite(PIN_LED_3, HIGH);
    delay(500);
    digitalWrite(PIN_LED_3, LOW);
    Stimulation();
    }
    if(START_STIMULATION != PRESSED){
    digitalWrite(PIN_LED_4, HIGH);
    delay(500);
    digitalWrite(PIN_LED_4, LOW);
    delay(500);
    
    }
  }
