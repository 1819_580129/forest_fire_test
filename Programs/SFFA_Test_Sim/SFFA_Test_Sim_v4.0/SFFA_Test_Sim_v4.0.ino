/*!--------------------------------------------------------------------
 * Node setup for SFFA project
 * Containing simulation mode for SSFA project 2018/2019 ESE
 * --------------------------------------------------------------------*/
///\file SFFA_Test_Sim_v4.0
///\brief Simulation file for the SFFA project
///\date 22-03-2019
///\author Jaap van Rooijen
///\version 4.0
/*!
 * \file SFFA_Test_Sim_v4.0.ino
 * \Release nr. 1 for simulation of the SFFA project
 * this sketch is made to have an indication of the test simulation
 * 
 * Code example used for OneWire DS18S20, DS18B20, and DS1822:
 * Temperature Example http://www.pjrc.com/teensy/td_libs_OneWire.html
 * Not used for this simulation
 * 
 * \date 22-03-2019
 * # Version 4
 * Created Mapped values for C02 and Temperature
 * Beautified code to improve readability
 * Added delay to be able to read the serial monitor
 * Borrowed image of Remko Welling of the IoT shield layout 
 * 
 *  
 * Version|Date        |Note
 * -------|------------|----
 * 1      | 11-03-2019 | First released version
 * 2      | 17-03-2019 | Created LED_Blink.cpp and .h for LED action call
 * 3      | 17-03-2019 | Implemented Potmeters
 * 4      | 22-03-2019 | See commend above 
 * 
 * Writen by Jaap van Rooijen
 * Student HAN ESE 583405
 * email: JJ.vanRooijen@student.han.nl
 * 
 * ## The use of this simulation
 * Below is an bullitpoint how to use this simulation
 * -Simulation starts with the press of the Red of Black button
 * + Black button: Can scroll true the differend simulation steps.
 * + Red button: Simulation goes directly to Case 3:
 * 
 * -Simulation steps contain
 * + Case 0: Nothing in progress, wait until one of buttons is pressed
 * + Case 1: Start of simulation, message on screen: "Simulatie gestart"
 * + Case 2: Higher risk of fire, message on screen: "!!WAARSCHUWING!!" "Verhoogde kans op brand!"
 * + Case 3: Fire detection by C02 level, message on screen: "!!ALARM!!" "Brand detectie op C02 gehalte" with value printed.
 * + Case 4: Fire confirmed by temperature lvl, message on screen: "!!ALARM!!" "Brand detectie op C02 en Temperatuur" 
 * 
 * ## LED status depending on Case nr.
 * | Case nr | LED 1  | LED 2  | LED 3     | LED 4     |
 * | ----:   | :----: | :----: | :----:    | :----:    |
 * | 1       | ON     | OFF    | OFF       | OFF       |
 * | 2       | 2 Hz   | OFF    | OFF       | OFF       |
 * | 3       | 4 Hz   | 4 Hz   | Value CO2 | OFF       |
 * | 4       | 10 Hz  | 10 Hz  | Value CO2 | Value Temp|
 */

  /*!
  # Shield layout

  
  \verbatim
  
    +-----------------------------+
    | [gr-1] [gr-2] [gr-2] [gr-2] | <- 4 grove connectors
    |                             |
    |   +--------+   +--------+   |  
    |   |        |   |        |   |  
    |   |   P1   |   |   P2   |   |  <- 2 potentiometers
    |   |        |   |        |   |  
    |   +--------+   +--------+   |  
    |                             |
    |     +----+       +----+     |  
    |     |Red |       |Blck|     |  <- 2 push button switches
    |     +----+       +----+     |  
    |                             |
    | [led1] [led3] [led2] [led4] |  <- 4 LEDs
    |                             |
    | [Dallas]                    |  <- Dallas one wire temperature sensor
    +-\                 /---------+
       \---------------/

  \author Remko Welling (remko.welling@han.nl)       
  \endverbatim

  
 */

// Release version of program
#define RELEASE 1

// Include 
#include "LED_Blink.h"

// Declaration for global variable
//variable of switch states
int switchBlackState;               // variable for reading the pushbutton status
int switchBlackStateLast = LOW;     // variable for reading last pushbutton status
int switchRedState;                 // variable for reading the pushbutton status
int switchRedStateLast   = LOW;     // variable for reading last pushbutton status
int buttonBlackState;               // variable for button state
int buttonRedState;                 // variable for button state

// Variable of cases
int CaseCount = 0; 

// Global exit action
// exit action can be created for an force stop.
bool EXITAction = true;

void Stimulation() {
  while (EXITAction){
// Standard delay otherwise cycletime is to short for reading messages
    delay(80); // remove/adjust when implemented in main file
    
// Read the state of the pushbutton value:
  switchRedState   = digitalRead(PIN_SWITCH_RED);
  switchBlackState = digitalRead(PIN_SWITCH_BLACK);
     
// Read rising trigger of switch BLACK
  if(switchBlackState != buttonBlackState){
      buttonBlackState = switchBlackState;
    if (buttonBlackState == PRESSED){
      CaseCount ++;
      Serial.println("Zwarte knop ingedrukt");
     }
    }
  switchBlackStateLast = switchBlackState; 
  
// Read rising trigger of switch RED
// Red switch posible to jump to CaseCount 3 from only from 0
  if(switchRedState != buttonRedState){
      buttonRedState = switchRedState;
    if ((buttonRedState == PRESSED)&&(CaseCount == 0)){
      CaseCount = 3;
      Serial.println("Rode knop ingedrukt.");
      }
     }
  switchRedStateLast = switchRedState;

// CaseCount may not exceed the count of 4
  if (((switchRedState == PRESSED)||(switchBlackState == PRESSED))&&(CaseCount > 4)){
    CaseCount = 0;
  }
//Go back to Normal operation
  if((switchRedState == PRESSED) && (switchBlackState == PRESSED)){
    CaseCount = 5;
       
  }

// Call function LEDBlinkAction to set LEDS depending CASE state
  LEDBlinkAction(CaseCount);
  }
}
