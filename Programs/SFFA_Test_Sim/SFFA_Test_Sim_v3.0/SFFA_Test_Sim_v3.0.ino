/*!--------------------------------------------------------------------
Node setup for SFFA project
Containing simulation mode for SSFA project 2018/2019 ESE
  --------------------------------------------------------------------*/
/*!
 * \file SFFA_Test_Sim_v2.0.ino
 * \brief sketch to try simulation of the SFFA
 * this sketch is made to have an indication of the test simulation
 * 
 * Code example used for OneWire DS18S20, DS18B20, and DS1822:
 * Temperature Example http://www.pjrc.com/teensy/td_libs_OneWire.html
 * \date 11-03-2019
 * \version 1
 * 
 * Version|Date        |Note
 * -------|------------|----
 * 1      | 11-03-2019 | First released version
 * 2      | 17-03-2019 | Created LED_Blink.cpp and .h for LED action call
 * 3      | 17-03-2019 | Implemented Potmeters
 */

  /*!
  # Shield layout

  
  \verbatim
  
    +-----------------------------+
    | [gr-1] [gr-2] [gr-2] [gr-2] | <- 4 grove connectors
    |                             |
    |   +--------+   +--------+   |  
    |   |        |   |        |   |  
    |   |   P1   |   |   P2   |   |  <- 2 potentiometers
    |   |        |   |        |   |  
    |   +--------+   +--------+   |  
    |                             |
    |     +----+       +----+     |  
    |     |Red |       |Blck|     |  <- 2 push button switches
    |     +----+       +----+     |  
    |                             |
    | [led1] [led3] [led2] [led4] |  <- 4 LEDs
    |                             |
    | [Dallas]                    |  <- Dallas one wire temperature sensor
    +-\                 /---------+
       \---------------/

       
  \endverbatim

  
 */

// release version of program
#define RELEASE 2

// Include 
#include "LED_Blink.h"

//Declaration for global variable
  //variable of switch states
  int switchBlackState;               // variable for reading the pushbutton status
  int switchBlackStateLast = LOW;     // variable for reading last pushbutton status
  int switchRedState;                 // variable for reading the pushbutton status
  int switchRedStateLast   = LOW;     // variable for reading last pushbutton status
  int buttonBlackState;
  int buttonRedState;

  //variable of cases
  int CaseCount = 0; 
  
  // Global exit action
  bool EXITAction = true;

void setup() {
  Serial.begin(9600);
  
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(PIN_LED_1, OUTPUT);
  pinMode(PIN_LED_2, OUTPUT);
  pinMode(PIN_LED_3, OUTPUT);
  pinMode(PIN_LED_4, OUTPUT);
  
    // initialize the pushbutton pin as an input:
  pinMode(PIN_SWITCH_BLACK, INPUT);
  pinMode(PIN_SWITCH_RED, INPUT);
  
}

void loop() {
  while (EXITAction){
    // read the state of the pushbutton value:
    switchRedState   = digitalRead(PIN_SWITCH_RED);
    switchBlackState = digitalRead(PIN_SWITCH_BLACK);
    
    Serial.println("Button read.");
    Serial.print("Value of CaseCount = ");
    Serial.print(CaseCount);
    Serial.println("  ");
    
      //Start of case senario depending on pressed buttons
      // read rising trigger of switch BLACK
      if(switchBlackState != buttonBlackState){
          buttonBlackState = switchBlackState;
        if (buttonBlackState == PRESSED){
          CaseCount ++;
        Serial.println("Black Button Pressed");
        }
      }
      switchBlackStateLast = switchBlackState; 
      
      //read rising trigger of switch RED
      //Red switch posible to jump to CaseCount 3 from only from 0
      if(switchRedState != buttonRedState){
          buttonRedState = switchRedState;
        if ((buttonRedState == PRESSED)&&(CaseCount == 0)){
          CaseCount = 3;
          Serial.println("RED Button Pressed.");
         }
      }
      switchRedStateLast = switchRedState; 
        // Reset of case counter after the 4th case
      if (((switchRedState == PRESSED)||(switchBlackState == PRESSED))&&(CaseCount > 4)){
        CaseCount = 0;
      }
          // Call function LEDBlinkAction to set LEDS depending CASE state
    LEDBlinkAction(CaseCount);
    }
}
