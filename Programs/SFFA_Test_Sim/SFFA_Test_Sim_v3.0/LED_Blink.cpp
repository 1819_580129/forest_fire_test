/*--------------------------------------------------------------------
* update discrition;
  --------------------------------------------------------------------*/

/// \file LED_Blink.cpp
/// \author Jaap van Rooijen HAN ESE minor

#include "LED_Blink.h"
#include "Arduino.h"

//Global var for potmeters
int potRedValue = 0;          // variable to store the value coming from the potentiometer
int potWhiteValue = 0;        // variable to store the value coming from the potentiometer

//Function for Red potmeter to led nr 3
void RedPotMeterToLED(){
    potRedValue = analogRead(PIN_POT_RED);
    analogWrite(PIN_LED_3, 1023 - potRedValue / 4);
    Serial.println("Value of Red Potmeter:");
    Serial.print(potRedValue);
    Serial.println("  ");
}

//Function for White potmeter to led nr 4
void WhitePotMeterToLED(){
    potWhiteValue = analogRead(PIN_POT_WHITE);
    analogWrite(PIN_LED_4, 1023 - potWhiteValue / 4);
    Serial.println("Value of White Potmeter:");
    Serial.print(potWhiteValue);
    Serial.println("  ");
}
    
// Function: LEDblinkAction
//LED flash without the use of delay

void LEDBlinkAction(int CaseCount){
  unsigned long currentMillis = millis();
  static unsigned long previousMillis = 0;
  const long interval = 1000;

 switch(CaseCount) {
  case 1:{
          digitalWrite(PIN_LED_1, HIGH);
        }
     break;
  case 2:
    if((currentMillis - previousMillis) >= (interval/2)){
          previousMillis = currentMillis;
          if(digitalRead(PIN_LED_1) == LOW){
            digitalWrite(PIN_LED_1, HIGH);
          }
          else{
            digitalWrite(PIN_LED_1, LOW);
          }
       }
    break;
  case 3:
    RedPotMeterToLED();
    if((currentMillis - previousMillis) >= (interval/4)){
          previousMillis = currentMillis;
          if(digitalRead(PIN_LED_1) == LOW){
            digitalWrite(PIN_LED_1, HIGH);
            digitalWrite(PIN_LED_2, LOW);
          }
          else{
            digitalWrite(PIN_LED_1, LOW);
            digitalWrite(PIN_LED_2, HIGH);
          }
       }
    break;
  case 4:
  WhitePotMeterToLED();
  RedPotMeterToLED();
     if((currentMillis - previousMillis) >= (interval/10  )){
          previousMillis = currentMillis;
          if(digitalRead(PIN_LED_1) == LOW){
            digitalWrite(PIN_LED_2, LOW);
            digitalWrite(PIN_LED_1, HIGH);
          }
          else{
            digitalWrite(PIN_LED_2, HIGH);
            digitalWrite(PIN_LED_1, LOW);
          }
       }     
    break;
  default :
  {
  digitalWrite(PIN_LED_1, LOW);
  digitalWrite(PIN_LED_2, LOW);
  digitalWrite(PIN_LED_3, LOW);
  digitalWrite(PIN_LED_4, LOW);
  }
 }
}
