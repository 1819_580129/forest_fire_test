/*--------------------------------------------------------------------
Node setup for SFFA project
Containing simulation mode for SSFA project 2018/2019 ESE
  --------------------------------------------------------------------*/
/*!
 * file SFFA_Test_Sim_v6.0
 * brief Simulation file for the SFFA project
 * date 22-03-2019
 * author Jaap van Rooijen
 * version 6.0
 * \file SFFA_Test_Sim_v6.0.ino
 * \Release nr. 3 for simulation of the SFFA project
 * 
 * \date 22-03-2019
 * # Version 6
 * Created Mapped values for C02 and Temperature
 * Beautified code to improve readability
 * Added delay to be able to read the serial monitor
 * Borrowed image of Remko Welling of the IoT shield layout 
 * 
 *  
 * Version|Date        |Note
 * -------|------------|----
 * 1      | 11-03-2019 | First released version
 * 2      | 17-03-2019 | Created LED_Blink.cpp and .h for LED action call
 * 3      | 17-03-2019 | Implemented Potmeters
 * 4      | 22-03-2019 | See commend above 
 * 5      | 01-04-2019 | Included Enums
 * 6      | 06-04-2019 | Moved switch var from global to local. Killed bug for direct start of simulation.
 * 
 * Writen by Jaap van Rooijen
 * Student HAN ESE 583405
 * email: JJ.vanRooijen@student.han.nl
 */


// Release version of program
#define RELEASE 3

// Include 
#include "LED_Blink.h"

// Declaration for global variable
// Variable of cases
int CaseCount = 0; 

void setup() {
  Serial.begin(9600);
  
// Initialize digital pin LED_BUILTIN as an output.
  pinMode(PIN_LED_1, OUTPUT);
  pinMode(PIN_LED_2, OUTPUT);
  pinMode(PIN_LED_3, OUTPUT);
  pinMode(PIN_LED_4, OUTPUT);
  
// Initialize the pushbutton pin as an input:
  pinMode(PIN_SWITCH_BLACK, INPUT);
  pinMode(PIN_SWITCH_RED, INPUT);
}

void loop() {
  //variable of switch states
static bool switchBlackState      = false;       // variable for reading the pushbutton status
static bool switchBlackStateLast  = RELEASED;    // variable for reading last pushbutton status
static bool switchRedState        = false;       // variable for reading the pushbutton status
static bool switchRedStateLast    = RELEASED;    // variable for reading last pushbutton status
static bool buttonBlackState      = true;        // variable for button state
static bool buttonRedState        = true;        // variable for button state

  // Standard delay otherwise cycletime is to short for reading messages
  delay(80); // remove/adjust when implemented in main file
    
  // Read the state of the pushbutton value:
  switchRedState   = digitalRead(PIN_SWITCH_RED);
  switchBlackState = digitalRead(PIN_SWITCH_BLACK);
       
  // Read rising trigger of switch BLACK
  if(switchBlackState != buttonBlackState){
      buttonBlackState = switchBlackState;
    if (buttonBlackState == PRESSED){
      CaseCount ++;
     }
    }
  switchBlackStateLast = switchBlackState; 
    
  // Read rising trigger of switch RED
  // Red switch posible to jump to CaseCount 3 from only from 0
  if(switchRedState != buttonRedState){
      buttonRedState = switchRedState;
    if ((buttonRedState == PRESSED)&&(CaseCount == 0)){
      CaseCount = 3;
      }
     }
  switchRedStateLast = switchRedState;
  
  // CaseCount may not exceed the count of 4
  if (((switchRedState == PRESSED)||(switchBlackState == PRESSED))&&(CaseCount > 4)){
    CaseCount = 0;
  }
    
  // Call function LEDBlinkAction to set LEDS depending CASE state
    LEDBlinkAction(CaseCount);
}
