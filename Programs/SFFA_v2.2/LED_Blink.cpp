/*!
 *
 * file LED_Blink.cpp
 * Functions for simulation the SFFA project
 * date 03-04-2019
 * author Jaap van Rooijen
 * version 2.0
 * 
 * \file LED_Blink.cpp
 * \release nr. 2 for simulation of the SFFA project
 * 
 * Writen by Jaap van Rooijen
 * Student HAN ESE 583405
 * email: JJ.vanRooijen@student.han.nl
 * 
 */


//For debugging of temp/h20/co2
//      Serial.print("H20 = ");
//      Serial.print(Data.actual_h2o);
//      Serial.println("  ");
//      Serial.print("C02 = ");
//      Serial.print(Data.actual_co2);
//      Serial.println("  ");
//      Serial.print("Temp = ");
//      Serial.print(Data.actual_temp);
//      Serial.println("  ");

#include "LED_Blink.h"
