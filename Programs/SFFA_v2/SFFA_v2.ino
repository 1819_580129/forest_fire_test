/*--------------------------------------------------------------------
  This file is part of the HAN IoT shield library.

  This code is free software:
  you can redistribute it and/or modify it under the terms of a Creative
  Commons Attribution-NonCommercial 4.0 International License
  (http://creativecommons.org/licenses/by-nc/4.0/) by
  Remko Welling (https://ese.han.nl/~rwelling/) E-mail: remko.welling@han.nl

  The program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  --------------------------------------------------------------------*/

/*!
   \file SFFA_TestSignalGenerator_003.ino
   \brief Sends packets on TTN using the HAN IoT Shield.
   this sketch is made to demonstrate the functionalities of the IoT Shield
   that is used in the IoT class of Embedded Systems. It delivers a library
   to allow studens to easily use the IoT shield and explore the interfaces
   that are used to work with the IoT shield.
   \author Remko Welling (remko.welling@han.nl)
   \date 14-1-2019
   \version 7

   Version|Date        |Note
   -------|------------|----
   8      |  4- 2-2019 | .........................................................................
   7      | 14- 1-2019 | .........................................................................
          |            | .........................................................................
   6      | 10- 1-2019 | .........................................................................
   5      | sdsdsdsdsd | First released version

*/
// include Headers
#include "LED_Blink.h"
#include <TheThingsNetwork.h>
#include <CayenneLPP.h>

// defenition of Defines
#define loraSerial Serial1
#define debugSerial Serial
#define freqPlan TTN_FP_EU868
#define co2_warning_value 500

TheThingsNetwork ttn(loraSerial, debugSerial, freqPlan);
const char *appEui = "/*invullen per node*/";
const char *appKey = "/*invullen per node*/";

typedef enum {MEASURE,
              ALARM_CHECK,
              CALCULATE_WAITING_TIME,
              WAITING,
              SEND_ALARM,
              SEND_IM_ALIVE

             } state_t;                              // state definities


// Declaration of global variable
int state = MEASURE;      //Init of state machine
bool SimulationState = false;
bool flag_cntSampleTime = false;
bool flag_cntImAliveTime = false;
bool flag_Alarm_Warning = false;
bool flag_Alarm = false;


// Declaration of structs
struct Data
{
  signed int actual_co2 = 400;
  signed int actual_h2o = 50;
  signed int actual_temp = 28;
  signed int min_h2o = 10;
  signed int max_h2o = 60;
  signed int min_temp = 0;
  signed int max_temp = 40;
  signed int min_sample_time = 5;
  signed int max_sample_time = 60;
  signed int waiting_time = min_sample_time;
         int caseState = 0; //0 = goed, 1 = warning, 2 = fikkie
} ;
typedef struct Data data;
struct Data *Pdata;
data Data;

struct Timer_vars
{
  //simulation mode
  char cnt1sec_sim = 2;           // 2 x 0.5 sec
  char cntSampleTime_sim;         // time counter sample time in seconds simulation
  char cnt1min_sim = 60;          // 60 x 1 sec
  char cnt4min_sim = 4;           // 4 x 1 min
  //normal mode
  char cnt1min = 120;         // 120 x 0.5 sec
  char cntSampleTime_norm;         // time counter sample time in minutes
  char cnt1hour = 60;         // 120 x 1 min
  char cnt4hour = 4;          // 4 x 1 hour
};
typedef struct Timer_vars Timer_Vars;
Timer_Vars TmrVars;




//Functions to call
void RedPotMeterToLED(bool SimulationState, struct Data *p){
 static int MappedpotRedValue = 0;         ///\ Mapped value depending SimulationState
        int potRedValueRaw    = 0;         ///\ Variable to store value from the potentiometer
  const int potRedMin         = 0;         ///\ Min range for Red potentiometer SimulationState false
  const int potRedMax         = 100;       ///\ Max range for Red potentiometer SimulationState false
  const int potRedMinSim      = 400;       ///\ Min range for Red potentiometer SimulationState true
  const int potRedMaxSim      = 5000;      ///\ Max range for Red potentiometer SimulationState true
  potRedValueRaw = analogRead(PIN_POT_RED);
  
  if (SimulationState == false){
      MappedpotRedValue = map(1023-potRedValueRaw, 0, 1023, potRedMin, potRedMax);
      //analogWrite(PIN_LED_3, 1023-potRedValueRaw/4); // LEDs are used for other function
      Data.actual_h2o = MappedpotRedValue;
  }
  else{
    MappedpotRedValue = map(1023-potRedValueRaw, 0, 1023, potRedMinSim, potRedMaxSim);
    //analogWrite(PIN_LED_3, 1023-potRedValueRaw/4); // LEDs are used for other function
    Data.actual_co2 = MappedpotRedValue;
    }
}

void WhitePotMeterToLED(bool SimulationState, struct Data *p){
 signed int MappedpotWhiteValue = 0;      ///\ Mapped value depending SimulationState
        int potWhiteValueRaw    = 0;      ///\ Variable to store value from the potentiometer
  const int potWhiteMin         = -20;    ///\ Min range for White potentiometer SimulationState false
  const int potWhiteMax         = 50;     ///\ Max range for White potentiometer SimulationState false
  const int potWhiteMinSim      = 35;     ///\ Min range for White potentiometer SimulationState true
  const int potWhiteMaxSim      = 500;    ///\ Max range for White potentiometer SimulationState true
  potWhiteValueRaw = analogRead(PIN_POT_WHITE);
  
  if (SimulationState == false){
    MappedpotWhiteValue = map(1023-potWhiteValueRaw, 0, 1023, potWhiteMin, potWhiteMax); 
    //analogWrite(PIN_LED_4, 1023-potWhiteValueRaw/4); // LEDs are used for other function
  }
  else{
    MappedpotWhiteValue = map(1023-potWhiteValueRaw, 0, 1023, potWhiteMinSim, potWhiteMaxSim);
    //analogWrite(PIN_LED_4, 1023-potWhiteValueRaw/4); // LEDs are used for other function
    }
  Data.actual_temp = MappedpotWhiteValue;
}

/*
   function that calculates the sensor sample time
   input:
   h2o => int, example: 80.3% = 80
   temp => int, example: 37.4°C = 37
   sample time => int
   output: sample time => int
*/

void calculate_sample_time(struct Data *p)
{
  float ratio_h2o, ratio_temp, ratio;
  signed int sample_time;

  if (p->actual_h2o < p->min_h2o) {
    p->actual_h2o = p->min_h2o; //overflow/underflow protection
  }
  if (p->actual_h2o > p->max_h2o) {
    p->actual_h2o = p->max_h2o; //overflow/underflow protection
  }
  if (p->actual_temp < p->min_temp) {
    p->actual_temp = p->min_temp; //overflow/underflow protection
  }
  if (p->actual_temp > p->max_temp) {
    p->actual_temp = p->max_temp; //overflow/underflow protection
  }

  //low humidity = high risk = low ratio, low temp = low risk = high ratio
  ratio_h2o = ((float)p->actual_h2o / (float)p->max_h2o);                 //calculate ratio relative humidity
  ratio_temp = (1 - ((float)p->actual_temp / (float)p->max_temp));        //calculate inverse ratio temperature

  ratio = (ratio_h2o + ratio_temp) / 2;                                   //average ratio

  sample_time = (int)((p->max_sample_time - p->min_sample_time) * ratio); //calculate ratio between min and max sample time
  sample_time = (int)(sample_time + p->min_sample_time);                  //add minimum sample time

  p->waiting_time = sample_time;
}


void init_timer() {
  // initialize timer1
  noInterrupts();           // disable all interrupts
  TCCR1A = 0;
  TCCR1B = 0;

  TCNT1 = 34286;            // preload timer 65536-16MHz/256/2Hz
  TCCR1B |= (1 << CS12);    // 256 prescaler
  TIMSK1 |= (1 << TOIE1);   // enable timer overflow interrupt
  interrupts();             // enable all interrupts
}

ISR(TIMER1_OVF_vect)        // 0.5 sec = 2Hz
{
  TCNT1 = 34286;            // preload timer          34286
  digitalWrite(PIN_LED_1, digitalRead(PIN_LED_1) ^ 1);

    char cnt1sec_sim = 2;           // 2 x 0.5 sec
    char cntSampleTime_sim;         // time counter sample time
    char cnt1min_sim = 60;          // 60 x 1 sec
    char cnt4min_sim = 4;           // 4 x 1 min

  if (SimulationState) {          //simulation mode
    if (TmrVars.cnt1sec_sim > 0) {
      TmrVars.cnt1sec_sim--;
    }
    if (TmrVars.cnt1sec_sim == 0) {
      TmrVars.cnt1sec_sim = 2;
      if (TmrVars.cnt1min_sim > 0) {
        TmrVars.cnt1min_sim--;
      }
      if (TmrVars.cntSampleTime_sim > 0) {
        TmrVars.cntSampleTime_sim--;
      }
    }
    if (TmrVars.cntSampleTime_sim == 0) {
      flag_cntSampleTime = true;                         //set flag to go to the next state
    }
    if (TmrVars.cnt1min_sim == 0) {
      TmrVars.cnt1min_sim = 60;

      if (TmrVars.cnt4min_sim > 0) {
        TmrVars.cnt4min_sim--;
      }
    }
    if (TmrVars.cnt4min_sim == 0) {
      TmrVars.cnt4min_sim = 4;
      flag_cntImAliveTime = true;                         //set flag to go to the next state
    }
  }
  else { //normal mode
    if (TmrVars.cnt1min > 0) {
      TmrVars.cnt1min--;
    }
    if (TmrVars.cnt1min == 0) {
      TmrVars.cnt1min = 120;

      if (TmrVars.cnt1hour > 0) {
        TmrVars.cnt1hour--;
      }
      if (TmrVars.cntSampleTime_norm > 0) {
        TmrVars.cntSampleTime_norm--;
      }
    }
    if (TmrVars.cntSampleTime_norm == 0) {
      flag_cntSampleTime = true;                         //set flag to go to the next state
    }
    if (TmrVars.cnt1hour == 0) {
      TmrVars.cnt1hour = 60;
      if (TmrVars.cnt4hour > 0) {
        TmrVars.cnt4hour--;
      }
    }
    if (TmrVars.cnt4hour == 0) {
      TmrVars.cnt4hour = 4;
      flag_cntImAliveTime = true;                         //set flag to go to the next state
    }
  }
}

/*
   function that checks whether there is a warning
   input:
   actual h2o => int, example: 80.3% = 80
   actual temp => int, example: 37.4°C = 37

   output: sample time => int
*/

void warningDetection(struct Data *p)
{
  if (p->actual_h2o < p->actual_temp) {                  //If humidity in percentage is lower than temperature in Celsius
    flag_Alarm_Warning = true;                              //raise warning
    Data.caseState = 1;
    digitalWrite(PIN_LED_4, HIGH);
  }
  else {
    flag_Alarm_Warning = false; // eigelijk hier niet op false zetten, bij taak uitvoeren laten doen (((na uitvoeren in main uitzetten!
    digitalWrite(PIN_LED_4, LOW);
    Data.caseState = 0;
  }

  if (p->actual_co2 > co2_warning_value) {               //If CO2 is higher then 500ppm
    flag_Alarm = true;                                      //raise alarm (FIRE)
    Data.caseState = 2;
    digitalWrite(PIN_LED_2, HIGH);
  }
  else {
    flag_Alarm = false; // eigelijk hier niet op false zetten, bij taak uitvoeren laten doen
    digitalWrite(PIN_LED_2, LOW);
    Data.caseState = 0;
  }
}



void setup() {
// init of timer and state machine
  init_timer();         //init timer 1 (hardware timer
  
// Initialize the pushbutton pin as an input:
  pinMode(PIN_SWITCH_BLACK, INPUT);
  pinMode(PIN_SWITCH_RED,   INPUT);
// Initialize digital pin LED_BUILTIN as an output.
  pinMode(PIN_LED_1, OUTPUT);
  pinMode(PIN_LED_2, OUTPUT);
  pinMode(PIN_LED_3, OUTPUT);
  pinMode(PIN_LED_4, OUTPUT);

//TTN
//Commented for debug if used remove comment
//  loraSerial.begin(57600);
//  debugSerial.begin(9600);
//  Serial.begin(9600);
//  debugSerial.println("-- STATUS");
//  ttn.showStatus();
//  debugSerial.println("-- JOIN");
//  ttn.join(appEui, appKey);
}

void loop() {
//variable of switch states
static bool switchBlackState      = false;       // variable for reading the pushbutton status
static bool switchRedState        = false;       // variable for reading the pushbutton status
static bool buttonBlackState      = true;        // variable for button state
static bool buttonRedState        = true;        // variable for button state
static bool switchBlackStateLast  = RELEASED;    // variable for reading last pushbutton status
static bool switchRedStateLast    = RELEASED;    // variable for reading last pushbutton status

// Read the state of the pushbutton value:
switchRedState   = digitalRead(PIN_SWITCH_RED);
switchBlackState = digitalRead(PIN_SWITCH_BLACK);

//Function to start simulation
 if((switchBlackState != buttonBlackState)&&(switchRedState != buttonRedState)){
      buttonBlackState = switchBlackState;
      buttonRedState = switchRedState;
        if ((buttonBlackState == PRESSED)&&(buttonBlackState == PRESSED)){
          if(SimulationState == true){
            digitalWrite(PIN_LED_3, LOW);
            SimulationState = false;
          }
          else{
            SimulationState = true;
            digitalWrite(PIN_LED_3, HIGH);
            Serial.println("Start of simulation");
            }
          }
       }
  switchBlackStateLast = switchBlackState;
  switchRedStateLast = switchRedState;

  //Debug of data
      Serial.print("H20 = ");
      Serial.print(Data.actual_h2o);
      Serial.println("  ");
      Serial.print("C02 = ");
      Serial.print(Data.actual_co2);
      Serial.println("  ");
      Serial.print("Temp = ");
      Serial.print(Data.actual_temp);
      Serial.println("  ");
      Serial.print("Fik drin status ");
      Serial.print(Data.caseState);
      Serial.println("  ");
//Start of state machine
  switch (state) {
    
    case MEASURE:{
      RedPotMeterToLED(SimulationState,Pdata);
      WhitePotMeterToLED(SimulationState,Pdata);
      state = ALARM_CHECK;
      Serial.println("Measure");
      }
    break;

    case ALARM_CHECK:{
      Serial.println("AlarmCheck");
      warningDetection(Pdata);
      if ((flag_Alarm_Warning == true)||(flag_Alarm == true)) {
        state = SEND_ALARM;
        Serial.println("Switch to SendAlarm");
        flag_Alarm_Warning = false;
         }
          else {
          state = CALCULATE_WAITING_TIME;
          Serial.println("Switch to CalsulatingWaitingTime");
          }
       }
    break;

    case CALCULATE_WAITING_TIME:{
      //calculate_sample_time(Pdata);
      Serial.println("CalculateWaitingTime");
      TmrVars.cntSampleTime_norm = (Pdata->waiting_time);
      TmrVars.cntSampleTime_sim = (Pdata->waiting_time);
      state = WAITING;
      }
    break;

    case WAITING:{
      Serial.println("Waiting");
      if (flag_cntSampleTime == true) {
        state = MEASURE;
        Serial.println("Switch to Measure");
        flag_cntSampleTime = false;
        break;

      }
      else if (flag_cntImAliveTime == true) {
        state = SEND_IM_ALIVE;
        Serial.println("Switch to SendImAlive");
        flag_cntImAliveTime = false;
        }
        // Waiting untill one of both flags is set
        // Jump for debug
        state = MEASURE;
      }
      break;

    case SEND_ALARM:{
      Serial.println("SendAlarm");
      //send_Alarm()
      //Reset Im Alive Counter
      state = WAITING;
      }
    break;

    case SEND_IM_ALIVE:{
      //send_ImAlive();
      Serial.println("SendImAlive");

      Serial.println("SwitchToWaiting");
      state = WAITING;
      }
    break;

  }
  delay(80); // remove/adjust when implemented in main file
}
