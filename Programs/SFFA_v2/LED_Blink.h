/*--------------------------------------------------------------------
  This file is part of the SFFA_test_Sim >v3.
  --------------------------------------------------------------------*/

/*!   
 * 
 * file LED_Blink.h
 * brief LED blink
 * date 03-04-2019
 * author Jaap van Rooijen
 * version 2.0
 *
 * \file LED_Blink.h
 * \Release nr. 2 for simulation of the SFFA project
 * this sketch is made to have an indication of the test simulation
 * 
 * Version|Date        |Note
 * -------|------------|----
 * 1.0    | 11-03-2019 | Created LED_Blink.cpp and .h for LED fucntion call
 * 1.1    | 17-03-2019 | See comment below
 * 2.0    | 03-04-2019 | See comment below
 *  
 *  # Version 1.1
 * Created Mapped values for C02 and Temperature
 * Beautified code to improve readability
 * Added delay to be able to read the serial monitor
 * Borrowed image of Remko Welling of the IoT shield layout 
 * 
 *  # Version 2.0
 * Changed use of potmeters depening by simulationState 
 * If simulation is not active Potmeter red   = Actual H20   Range(0-100)
 * If simulation is not active Potmeter white = Actual Temp  Range(-20/50)
 * If simulation is active Potmeter red       = Actual C02   Range(400/5000)
 * If simulation is active Potmeter white     = Actual Temp  Range(35/500)
 * Values manipulated by the potmeters are writen in the struct 'Data'
 * Created enums for simulation state for readability improvement
 * 
 * ## The use of this simulation
 * Below is an bullitpoint how to use this simulation
 * +Simulation starts with the press of the Red or Black button
 * ++ Black button: Can scroll true the differend simulation steps.
 * ++ Red button: Simulation goes directly to Case 3:
 * 
 * Simulation steps contain
 * + Case 0: Nothing in progress, wait until one of buttons is pressed
 * + Case 1: Start of simulation, potmeter Red
 * + Case 2: Higher risk of fire
 * + Case 3: Fire detection by C02 level
 * + Case 4: Fire confirmed by temperature lvl
 * 
 * ## LED status depending on Case nr.
 * | Case nr | LED 1  | LED 2  | LED 3     | LED 4     |
 * | ----:   | :----: | :----: | :----:    | :----:    |
 * | 0       | OFF    | OFF    | OFF       | OFF       |
 * | 1       | ON     | OFF    | OFF       | OFF       |
 * | 2       | 2 Hz   | OFF    | OFF       | OFF       |
 * | 3       | 4 Hz   | 4 Hz   | Value CO2 | OFF       |
 * | 4       | 10 Hz  | 10 Hz  | Value CO2 | Value Temp|
 */

  /*!
  # Shield layout

  
  \verbatim
  
    +-----------------------------+
    | [gr-1] [gr-2] [gr-2] [gr-2] | <- 4 grove connectors
    |                             |
    |   +--------+   +--------+   |  
    |   |        |   |        |   |  
    |   |   P1   |   |   P2   |   |  <- 2 potentiometers
    |   |        |   |        |   |  
    |   +--------+   +--------+   |  
    |                             |
    |     +----+       +----+     |  
    |     |Red |       |Blck|     |  <- 2 push button switches
    |     +----+       +----+     |  
    |                             |
    | [led1] [led3] [led2] [led4] |  <- 4 LEDs
    |                             |
    | [Dallas]                    |  <- Dallas one wire temperature sensor
    +-\                 /---------+
       \---------------/

  author Remko Welling (remko.welling@han.nl)       
  \endverbatim

  
 */
#ifndef __LED_BLINK_H_
#define __LED_BLINK_H_

///define for LEDs
#define PIN_LED_1    3     ///< Led 1 is red and connected to arduino pin 3
#define PIN_LED_2    4     ///< Led 2 is red and connected to arduino pin 4
#define PIN_LED_3    5     ///< Led 3 is green and connected to arduino pin 5
#define PIN_LED_4    6     ///< Led 4 is green and connected to arduino pin 6

///define of the potmeters red and white
#define PIN_POT_RED      A0         ///< Potmeter 1 with red knob is connected to Arduino pin A0
#define PIN_POT_WHITE    A1         ///< Potmeter 2 with white knob is connected to Arduino pin A1

///define for pushbutton
#define PIN_SWITCH_BLACK 8     ///< Black switch is connected to pin Arduino pin 8
#define PIN_SWITCH_RED   9     ///< Red switch is connected to pin Arduino pin 9
#define RELEASED HIGH          ///< represent activity of pushbutton, Released is true
#define PRESSED  LOW           ///< represent activity of pushbutton, Pressed is false

#endif //__LED_BLINK_H_
